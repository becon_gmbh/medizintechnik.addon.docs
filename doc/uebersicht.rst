#########
Übersicht
#########

Durch das MedTec Add-on wird eine neue Objekttypgruppe "MedTec" erstellt. Dieser Objekttypgruppe werden drei Arten von Objekttypen zugeordnet:

* `Einweisung <objekttypen.html#objekttyp-einweisung>`_
* `Station/Fachabteilung <objekttypen.html#objekttyp-station-fachabteilung>`_
* `Medizingerät <objekttypen.html#objekttyp-art-medizingerat>`_

Das MedTec-Add-on installiert mehrere :doc:`reports` zur erweiterten Auswertung und Dokumentation der Medizingeräte.

Außerdem wird mit der :doc:`einweisungsmatrix` ein Überblick über die vorhanden Einweisungen gegeben.