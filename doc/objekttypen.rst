#################
MedTec-Objekttypen
#################

Das MedTec-Add-on führt drei neue Arten von Objekttypen ein, die alle in der Objekttypgruppe "MedTec" eingeordnet werden:

******************
Objekttyp Einweisung
******************

.. image:: img/medtec_100x100.jpg
     :class: floatright

Der Objekttyp "Einweisung" wird genutzt um zu dokumentieren, welcher Mitarbeiter für welche Geräteart unterwiesen wurde. Dazu können natürlich auch Datum und Protokoll dokumentiert werden.

**Allgemein** *(i-doit Standardkategorie)* 
    Gehört zu jedem Objekt.
**Einweisung** 
    Dies ist eine Multivalue-Kategorie, in der alle Einweisungen für die jeweilige Hersteller/Modell Kombination hinterlegt werden können.
    Es kann ein Unterweiser und mehrere unterwiesene Personen ausgewählt werden. Außerdem kann dokumentiert werden, ob es eine Ersteinweisung ist (Ja/Nein), das Datum der Unterweisung, der Status (durchgeführt, geplant, o.ä.) und ein Ablaufdatum. Unter "schriftlicher Nachweis" kann eine Datei hochgeladen werden, die in i-doit Revisionssicher abgelegt wird.
**Modell**  *(i-doit Standardkategorie)*
    Diese Kategorie Modell gehört zum Standard von i-doit und muss ausgewählt werden, damit die Automatische zuweisung von Einweisungen an Medizinprodukte funktioniert.


*********************
Objekttyp Station/Fachabteilung
*********************

.. image:: img/medtec_100x100.jpg
     :class: floatright

Mit diesem Objekttypen werden Stationen bzw. Fachabteilungen mit ausgeliefert, wie sie in den meisten Krankenhäusern üblich sind. Dieser Objekttyp dient selbst als Standort und hat natürlich selbst auch die Standort-Kategorie, damit er sich z.B. unterhalb eines Gebäudes in der Standort-Struktur unterbringen lässt.Folgende Objekte werden bei der Installation angelegt:

* Chirurgie
* Dermatologie
* Geriatrie
* Innere Medizin
* Intensivmedizin
* Neurologie
* Notaufnahme
* Orthopädie
* Pädiatrie
* Pneumologie
* Psychiatrie
* Strahlenheilkunde

Im Standard gehören folgende Kategorien zum Objekttypen Station/Fachabteilung:

**Allgemein** *(i-doit Standardkategorie)*
    Gehört zu jedem Objekt.
**Standort**   *(i-doit Standardkategorie)*
    Die i-doit Standardkategorie dient dazu, einen übergeordneten Standort für das Objekt festzulegen. Es hat direkte auswirkungen auf die Standort-Ansicht.


********************
Objekttyp-Art Medizingerät
********************

.. image:: img/medtec_100x100.jpg
     :class: floatright

Es werden mehrere Objekttypen derselben Art mit dem Add-on ausgeliefert, um die Übersichtlichkeit in den Listenansichten zu wahren.
Folgende Objekttypen gehören dazu:

| |symbol| Absorptionsmessgerät
| |symbol| Alarmeinrichtung
| |symbol| Analysegerät
| |symbol| Aphereresegerät
| |symbol| Beatmungsgerät
| |symbol| Computertomograph
| |symbol| Defibrillator
| |symbol| Dilutor
| |symbol| Dispensor
| |symbol| Elektroenzephalograph
| |symbol| Flowmeter
| |symbol| Hämodialysator
| |symbol| Informationssystem
| |symbol| Kamera
| |symbol| Laser
| |symbol| Luftreinigungsgerät
| |symbol| Mikroskop
| |symbol| Monitor (med)
| |symbol| Oberflächendarstellungs-System
| |symbol| Oberflächenspule
| |symbol| Pumpe
| |symbol| Reanimationsgerät
| |symbol| Rekorder
| |symbol| Rettungshubschrauber
| |symbol| Röntgenaufnahme-/Durchleuchtungsgerät
| |symbol| Schrittmacher
| |symbol| Simulator
| |symbol| Sonde
| |symbol| Spektralphotometer
| |symbol| Sterilisator
| |symbol| Stimulator
| |symbol| Ultraschallgerät
| |symbol| Verdichter
| |symbol| Waage
| |symbol| Waschautomat
| |symbol| Wärmegerät
| |symbol| Wärmetauscher
| |symbol| Xenon-Clearance
| |symbol| Zentrifuge
 

**Allgemein** *(i-doit Standardkategorie)*
    Gehört zu jedem Objekt.
**Buchhaltung**  *(i-doit Standardkategorie)*
    In dieser i-doit Standardkategorie können Werte aus der Buchhaltung (z.B. Inventarnummer, Anschaffungsdatum, Garantiedaten) dokumentiert werden.
**Handbuchzuweisung** (i-doit Standardkategorie)
    Diese Kategorie bietet die Möglichkeit Handbücher für das Medizinprodukt zu dokumentieren. Dies bietet sich insbesondere an um diese für alle Mitarbeiter einsehbar zu machen, wie es in §4 Nr. 7 MPBetreibV gefordert ist.
**Kontaktzuweisung** *(i-doit Standardkategorie)* 
    Über die Kategorie Kontaktzuweisung können dem Medizinprodukt Kontakte und Rollen zugewiesen werden. Das MedTec-Add-on bringt die neue Rolle "Verantwortlicher nach §5 MPG" mit.
**Medizingerät**  
    In dieser Kategorie ist es möglich die UMDNS-Bezeichnnung, Typ, Loscode und die betriebliche Identifikationnummer von Medizingeräten zu dokumentieren. Für die UMDNS-Nummer gibt es eine Suche, die nach der Eingabe von mindestens 3 Zeichen startet. Weiterhin sind folgende Attribute dokumentierbar:
* Anschaffungsjahr
* Kennnummer der benannten Stelle
* Risikoklasse ( I, IIa, IIb oder III)
* Beleg für Funktionstest (nach §10 Absatz 1 MPBetreibV)
* Fristen bzw. Zyklus für 
    * Sicherheitstechnische Kontrolle
    * Messtechnische Kontrolle
    * Wartung
.. image:: img/cat_med.PNG


**Modell**  *(i-doit Standardkategorie)*
    Diese Kategorie Modell gehört zum Standard von i-doit und muss ausgewählt werden, damit die Automatische zuweisung von Einweisungen an Medizinprodukte funktioniert.
**Standort**  *(i-doit Standardkategorie)*
    Die i-doit Standardkategorie dient dazu, einen übergeordneten Standort für das Objekt festzulegen. Es hat direkte auswirkungen auf die Standort-Ansicht.
**Report: Medizinproduktebuch**
    In diesem variablen Report werden Werte dargestellt, die in einem Medizinproduktebuch nach MPBereibV vorhanden sein sollten.
**Report: zugehörige Einweisung** 
    In diesem variablen Report werden alle Einträge der Multivalue Kategorie "Einweisung" des zugehörigen Einweisungs-Objekts dargestellt. Dieses wird über die Hersteller/Modell Kombination automatisch ermittelt.
**Termine**
    In dieser Multi-Value Kategorie können Termine für die Messtechnische- und Sicherheitstechnische Kontrolle sowie Wartungen von Medizingeräten dokumentiert werden. Dazu können ein Servicebericht und ein Ergebnis hochgeladen werden. Außerdem gibt es eine Verknüpfung zum „Durchführenden“, womit eine Person oder auch eine Fremdfirma (Organisation) verlinkt werden kann.

.. image:: img/cat_termin_list.PNG

.. image:: img/cat_termin.PNG

.. |symbol| image:: img/icons/default.png