#########
Changelog
#########

***********
Version 2.1
***********

Deutsch
=======
::

	[Neue Funktion] Einweisungsmatrix
	[Verbesserung]  Umbenennung des Add-ons
	[Bug]           Listeneditierung funktionierte nicht


***********
Version 2.0
***********

Deutsch
=======
::

	[Neue Funktion] Neuer Objekttyp "Einweisung"
	[Neue Funktion] Neue Kategorie "Termine"
	[Neue Funktion  Neue Reports für Medizinproduktebuch und Medizingeräte Bestandsverzeichnis
	[Verbesserung]  Die Kategorie "Handbuch" wird nun jedem Medizingerät hinzugefügt
	[Verbesserung]  Weitere Attribute in Kategorie Medizingerät um MPBetreibV relevante Werte zu dokumentieren
