#################
Einweisungsmatrix
#################

Das MedTec-Add-on fügt unter Extras den Menüpunkt "MedTec" ein. Dort findet man die Einweisungsmatrix.

In dieser werden alle Medizingeräte und Personen angezeigt. Für die Geräte gibt es am oberen Bildrand eine Filterfunktion.
Die Farbe Grün steht für eine gültige Einweisung. Durch die Symbolik ist zu erkennen, ob es eine Ersteinweisung ist oder nicht und ob diese bald abläuft.
Die Farbe Rot steht für "nicht eingewiesen" oder abgelaufene Einweisungen. Mehr Aufschluss über die Symbolik gibt die Legende am linken Bildrand der Einweisungsmatrix.

.. image:: img/Einweisungsmatrix.PNG

Die Zahl neben der Sanduhr gibt jeweils an, wie viele Tage die Einweisung noch gültig ist (grün hinterlegt und positiv) oder wie viele Tage die Einweisung schon abgelaufen ist (rot hinterlegt und negativ).

.. |symbol| image:: img/icons/default.png