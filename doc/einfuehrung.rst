##########
Einführung
##########

Das Add-on MedTec erweitert i-doit um Objekttypen für die Dokumentation von medizinischen Geräten. Ehemals wurde das Add-on "DIMDI" nach dem deutschen Institut für Medizinische Dokumentation und Information (DIMDI) benannt, welches mit dem Universal Medical Device Nomenclature System (UMDNS) einen Katalog zur Klassifizierung von medizinischen Geräten zur Verfügung stellt. i-doit ist in Kombination mit dem Add-on MedTec in der Lage diese medizinischen Geräte zu dokumentieren.

Medizinische Geräte wie MRT, CT und PACS zunehmend auf Standard IT und führen zur Verschmelzung von Medizin IT und Technik IT. Zudem stellen immer mehr  Krankhäuser und medizinische Einrichtungen auf eine umfassende Dokumentation ihrer Umgebung um, die auch den Grundstein für eine Risikoeinschätzung nach der ISO 80001 Norm legt. Beide Faktoren machen die Dokumentation von medizinischen Geräten in einer Standard CMDB sinnvoll.

Zusammgefasst bietet das folgende Vorteile:

* Unterstützung und Dokumentation nach Medizinprodukte Betreiberverordnung (MPBetreibV) und Medizinproduktegesetz (MPG)
* Dokumentation der Medizingeräte in der CMDB als Datengrundlage für das Ticketsystem
* Dokumentation der Medizingeräte als Grundlage für Risikoeinschätzungen z.B. nach ISO 80001, ISO 27001 oder für kritische Infrastrukturen mithilfe des ISMS-Add-on