#######
Reports
#######

Durch das MedTec-Add-on werden mehrere Reports installiert, die der Reports-Kategorie "MedTec" zugeordnet werden. Dies umfasst aktuell einen "normalen" und zwei "variable" Reports:

**Report: Medizinprodukte Bestandsverzeichnis**
    Dieser Report listet alle Medizinprodukte auf. Die Ausgegeben Attribute richten Sich dabei nach den Anforderungen aus der MPBetreibV und sind im einzelnen:
* Bezeichnung
* Bezeichnung (UMDNS)
* Typ
* Loscode
* Seriennummer
* Anschaffungsjahr
* Verantworlich nach §5 MPG
* Anschrift des Verantworlichen nach §5 MPG
* Kennnummer der benannten Stelle
* Betriebliche Identifikationsnummer
* Standort-Pfad
* Sicherheitstechnische Kontrolle (Zyklus + Einheit)

Die beiden variablen Reports sind alle über gleichnamige benutzerdefinierte Kategorien in den :doc:`objekttypen` eingebunden und dort beschrieben.