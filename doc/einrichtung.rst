###########
Einrichtung
###########

Das MedTec Add-on bringt eine Vielzahl von neuen Objekttypen mit. Dabei werden 3 grundsätzliche Objekttypen unterschieden:

* Einweisung
* Station/Fachabteilung
* Medizinprodukte

Als erster Schritt in der Einrichtung des Add-ons empfiehlt es sich, die Stationen/Fachabteilungen an die Unternehmensstruktur anzupassen. Dazu können die ausgelieferten Objekte umbenannt und Standorte vergeben werden. Das Ergebnis könnte wie folgt aussehen:

.. image:: img/einrichtung1.PNG

Im zweiten Schritt kann in der Verwaltung entweder über den Quick Configuration Wizard oder über die Objekttyp-Konfiguration eine Anpassung der Medizinprodukte-Objekttypen vorgenommen werden.
Welche Kategorien die Objekttypen bereits im Standard haben, können sie in den `Objekttypen <objekttypen.html>`_ nachlesen.


.. |pfeil| unicode:: U+23F5