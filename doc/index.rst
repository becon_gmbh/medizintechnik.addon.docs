######################################################
Willkommen zur Dokumentations des i-doit Add-Ons MedTec
######################################################

Das MedTec Add-On unterstützt bei der Dokumentation von Medizinprodukten.

------------

.. toctree::
    :maxdepth: 1
    :caption: Erste Schritte
    
    einfuehrung
    systemvoraussetzungen
    installation
    einrichtung


.. toctree::
    :maxdepth: 1
    :caption: Funktionsübersicht
    
    uebersicht
    objekttypen
    einweisungsmatrix
    reports
    

.. toctree::
    :maxdepth: 1
    :caption: Historie
    
    changelog 


##################
License
##################

`becon`_ © 2013-2020 becon GmbH

.. _becon: LICENSE.html